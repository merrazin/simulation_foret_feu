Ce projet est une simulation de feu de forêt.
Création d'une matrice. Chaque case représente un arbre (états : normal, en feu, éteint, brulé). A chaque temps d'horloge, un arbre en feu peut s'éteindre ou bien propager aux arbres voisins.

Ce program a été rédigé par Lucas(expert), Adnane(CTO), PE(CEO) et Nathan(Secrétaire) avec la supervision de M.URO(boss).
